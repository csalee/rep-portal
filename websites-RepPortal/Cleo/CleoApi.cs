﻿namespace websites_RepPortal.Cleo
{
    using System;
    using System.Configuration;
    using System.Linq;
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using IObject.Common.Account;
    using IObject.Common.Interfaces;
    using IObject.Core;

    public class CleoApi
    {
        private static Binding mServiceHttpBinding;
        private readonly EndpointAddress mServiceEndpoint;
        private string mApiKey;

        public CleoApi()
        {
            mServiceEndpoint =
                new EndpointAddress(new Uri(ConfigurationManager.AppSettings["cleoAccountHandlerEndpoint"]));

            var myEnv = ConfigurationManager.AppSettings["MyEnv"];
            if (myEnv == "Prod")
            {
                mServiceHttpBinding = new BasicHttpBinding(BasicHttpSecurityMode.Transport)
                {
                    MaxReceivedMessageSize = 2000000000,
                    SendTimeout = TimeSpan.FromMinutes(2)
                };
            }
            else
            {
                mServiceHttpBinding = new WSHttpBinding(SecurityMode.None) {MaxReceivedMessageSize = int.MaxValue};
            }
        }

        public AccountSummary GetAccount()
        {
            var tAccountAccessRequest = GetAccountAccessRequest();
            using (var tChannelWrapper = new ChannelWrapper<IAccountHandler>(mServiceHttpBinding, mServiceEndpoint))
            {
                var tAccountHandlerClient = tChannelWrapper.Channel;

                var tResponse = tAccountHandlerClient.GetAccountsummary(tAccountAccessRequest);

                if (tResponse.ResultCode != 0)
                {
                    Exception tInner = null;
                    if (tResponse.ErrorList != null)
                    {
                        tInner =
                            new AggregateException(
                                tResponse.ErrorList.Select(
                                    s => new Exception($"{s.Field}: {s.Error}")));
                    }

                    throw new Exception($"Invalid response result code '{tResponse.ResultCode}'",
                        tInner);
                }

                return tResponse.ResponseData;
            }
        }

        private static AccountAccessRequest GetAccountAccessRequest()
        {
            var tAccountAccessRequest = new AccountAccessRequest {IPAddr = ""};

            int tAccountId;
            if (!int.TryParse(ConfigurationManager.AppSettings["cleoAccountID"], out tAccountId))
            {
                throw new ConfigurationErrorsException("cleoAccountID must be supplied as an integer");
            }
            tAccountAccessRequest.AccountID = tAccountId;

            short tWebsiteId;
            if (!short.TryParse(ConfigurationManager.AppSettings["cleoWebsiteID"], out tWebsiteId))
            {
                throw new ConfigurationErrorsException("cleoWebsiteID must be supplied as an integer");
            }
            tAccountAccessRequest.WebsiteID = tWebsiteId;

            tAccountAccessRequest.ApiKey = ConfigurationManager.AppSettings["cleoApiKey"];

            return tAccountAccessRequest;
        }
    }
}