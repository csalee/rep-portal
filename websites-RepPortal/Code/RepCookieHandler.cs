﻿namespace websites_RepPortal.Code
{
    using System;
    using System.Linq;
    using System.Web;
    using IObject.Common.Rep;
    using IObject.Core;

    public class RepCookieHandler
    {
        private static readonly string _repTokenKey = "rt";
        private static readonly string _repIdKey = "ri";
        private readonly string _domain;
        private readonly HttpRequest _request;
        private readonly HttpResponse _response;
        private readonly string _secureDomain;
        private readonly short _websiteID;
        private int? _repId;
        private string _token;

        public RepCookieHandler(HttpRequest request, HttpResponse response, short WebsiteID, string Domain,
            string SecureDomain)
        {
            _request = request;
            _response = response;
            _websiteID = WebsiteID;
            _domain = Domain;
            _secureDomain = SecureDomain;

            ReadCookies();
        }

        public RepAccessRequest BuildRepAccessRequest()
        {
            var req = new RepAccessRequest();
            req.AuthenticationToken = _repTokenKey;
            req.RepID = _repId;
            return req;
        }

        public void SetRepTokenCookie(string token)
        {
            if (token == null)
            {
                return;
            }

            SetCookie(_repTokenKey, 30, token, true, false);
        }

        public void SetRepIdCookie(int id)
        {
            SetCookie(_repIdKey, 30, id, true, false);
        }

        public void SetCookie(string key, int? days, object value, bool httpOnly = true, bool secure = true)
        {
            if (_response == null)
            {
                return;
            }

            if (value == null)
            {
                SetCookie(key, -10, string.Empty, httpOnly, secure);
                return;
            }

            var newCookie = new HttpCookie(key);

            if (days.HasValue)
                newCookie.Expires = DateTime.Now.AddDays(days.Value);

            newCookie.HttpOnly = httpOnly;
            newCookie.Value = value.ToString();
            newCookie.Secure = secure;

            if (secure)
                newCookie.Domain = _secureDomain;
            else
                newCookie.Domain = _domain;


            _response.Cookies.Add(newCookie);
        }

        public void ReadCookies()
        {
            var tokenCookie = GetCookie(_repTokenKey);

            if (tokenCookie != null)
            {
                var token = AlwaysConvert.ToString(tokenCookie.Value, string.Empty);
                if (!string.IsNullOrWhiteSpace(token))
                    _token = token;
            }

            var repCookie = GetCookie(_repIdKey);

            if (repCookie != null)
            {
                var tmp = AlwaysConvert.ToInt(repCookie.Value, int.MinValue);

                if (tmp != int.MinValue && tmp > 0)
                    _repId = tmp;
            }
        }

        public HttpCookie GetCookie(string CookieName)
        {
            if (_request == null || _response == null || _response.Cookies == null || _response.Cookies.AllKeys == null)
                return null;

            var cookie = _response.Cookies.AllKeys.Contains(CookieName)
                ? _response.Cookies[CookieName]
                : _request.Cookies[CookieName];

            return cookie;
        }

        public string GetCookieValue(string CookieName)
        {
            var cookie = GetCookie(CookieName);
            if (cookie == null)
                return null;

            return cookie.Value;
        }

        public void ClearRepCookies()
        {
            SetCookie(_repTokenKey, -10, "null", true, true);
            SetCookie(_repIdKey, -10, "null", true, true);
        }
    }
}