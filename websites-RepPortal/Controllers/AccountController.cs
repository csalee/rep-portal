﻿namespace websites_RepPortal.Controllers
{
    using System;
    using System.Reflection;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using Code;
    using IObject.Common;
    using IObject.Common.Rep;
    using IObject.Core;
    using log4net;
    using Microsoft.AspNet.Identity;
    using Microsoft.Owin.Security;
    using Models;
    using LogManager = IObject.Core.LogManager;

    [Authorize]
    public class AccountController : BaseController
    {
        private static LogManager log = new LogManager(typeof(AccountController));
        // private static ILog log = LogManager.GetLogger(typeof(AccountController));
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;
        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            log.Debug("Login Page Called");
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            try
            {
                log.Debug("Attempted Login. ModelState.IsValid: " + ModelState.IsValid);
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                var context = System.Web.HttpContext.Current;
                request = context.Request;
                response = context.Response;
                response.BufferOutput = true;

                var aar = new RepAccessRequest
                {
                    RepUserName = AlwaysConvert.ToString(model.Email),
                    RepPassword = AlwaysConvert.ToString(model.Password)
                };

                var res = ExecuteRepAction(aar, (client, req) => client.AuthenticateRep(req));
                if (res.ResultCode == (int) Lookup.AccountRequestResultCode.Success)
                {
                    log.Info("Login Success");
                    var ident = new ClaimsIdentity(
                        new[]
                        {
                            new Claim(ClaimTypes.NameIdentifier, model.Email),
                            new Claim(
                                "http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider",
                                "ASP.NET Identity", "http://www.w3.org/2001/XMLSchema#string"),
                            new Claim(ClaimTypes.Name, model.Email)
                        }, DefaultAuthenticationTypes.ApplicationCookie);

                    HttpContext.GetOwinContext()
                        .Authentication.SignIn(new AuthenticationProperties {IsPersistent = false}, ident);

                    cookieManager = new RepCookieHandler(request, response, 6, domain, secureDomain);
                    cookieManager.SetRepTokenCookie(res.Token);
                    cookieManager.SetRepIdCookie(res.RepID ?? 0);
                    return RedirectToAction("RepAccounts", "Home");
                }
                else
                {
                    ModelState.AddModelError("Login Error", "Error Logging In: " + res.ResultCode);
                    log.Error("Login Not Successful: " + res.ResultCode + " ErrorTxt: " +  res.ErrorTxt);
                }

                cookieManager.ClearRepCookies();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Login Exception", ex);
                log.Error("Error in Login: ", ex);
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            cookieManager = new RepCookieHandler(request, response, 6, domain, secureDomain);
            cookieManager.ClearRepCookies();
            return RedirectToAction("Index", "Home");
        }
    }
}