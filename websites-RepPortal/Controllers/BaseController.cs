﻿namespace websites_RepPortal.Controllers
{
    using System;
    using System.Configuration;
    using System.Diagnostics;
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using System.Web;
    using System.Web.Mvc;
    using Code;
    using IObject.Account;
    using IObject.Common;
    using IObject.Common.Account;
    using IObject.Common.Interfaces;
    using IObject.Common.Rep;
    using IObject.Core;

    public class BaseController : Controller
    {
        public delegate AccountAccessResponse<T> ExecuteAccountActionDelegate<T>(
            IAccountHandler client, AccountAccessRequest aar);

        public delegate RepAccessResponse<T> ExecuteRepActionDelegate<T>(IRepHandler client, RepAccessRequest aar);

        protected static Binding _binding = null;
        protected static string domain = ConfigurationManager.AppSettings["Domain"];
        protected static string secureDomain = ConfigurationManager.AppSettings["SecureDomain"];
        protected EndpointAddress _endpoint;
        protected AccountCookieHandler aCookieManager;
        protected RepCookieHandler cookieManager;
        private EndpointAddress mServiceEndpoint = null;
        protected HttpRequest request = null;
        protected HttpResponse response = null;
        // GET: Base
        protected RepAccessResponse<T> ExecuteRepAction<T>(RepAccessRequest aar, ExecuteRepActionDelegate<T> executor,
            bool ByPassAuthenticationCheck = true)
        {
            cookieManager = new RepCookieHandler(request, response, 6, domain, secureDomain);
            _endpoint = new EndpointAddress(new Uri(ConfigurationManager.AppSettings["RepHandlerEndpoint"]));
            SetBinding();

            using (var wrapper = new ChannelWrapper<IRepHandler>(_binding, _endpoint))
            {
                var client = wrapper.Channel;
                var res = executor(client, aar);

                if (res.ResultCode != (int) Lookup.AccountRequestResultCode.Success)
                {
                    throw new Exception($"Invalid response result code '{res.ResultCode}'");
                }

                return res;
            }
        }

        protected AccountAccessResponse<T> ExecuteAccountAction<T>(AccountAccessRequest aar,
            ExecuteAccountActionDelegate<T> executor, bool ByPassAuthenticationCheck = true)
        {
            var tStopwatch = new Stopwatch();
            tStopwatch.Start();
            long tAccountActionTime;

            aCookieManager = new AccountCookieHandler(request, response, 6, domain, secureDomain);
            _endpoint = new EndpointAddress(new Uri(ConfigurationManager.AppSettings["AcctHandlerEndpoint"]));
            SetBinding();

            using (var wrapper = new ChannelWrapper<IAccountHandler>(_binding, _endpoint))
            {
                var client = wrapper.Channel;

                tAccountActionTime = tStopwatch.ElapsedMilliseconds;
                var res = executor(client, aar);
                tAccountActionTime = tStopwatch.ElapsedMilliseconds - tAccountActionTime;

                if (res.ResultCode == (int) Lookup.AccountRequestResultCode.Success)
                {
                    aCookieManager.SetAccountCookies(res);
                }
                else
                {
                    throw new Exception($"Invalid response result code '{res.ResultCode}'");
                }

                return res;
            }
        }

        private void SetBinding()
        {
            var myEnv = ConfigurationManager.AppSettings["MyEnv"];
            if (myEnv == "Prod")
            {
                _binding = new BasicHttpBinding(BasicHttpSecurityMode.Transport)
                {
                    MaxReceivedMessageSize = 2000000000,
                    SendTimeout = TimeSpan.FromMinutes(2)
                };
            }
            else
            {
                _binding = new WSHttpBinding(SecurityMode.None) {MaxReceivedMessageSize = int.MaxValue};
            }
        }
    }
}