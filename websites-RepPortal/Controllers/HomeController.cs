﻿namespace websites_RepPortal.Controllers
{
    using System.Web.Mvc;
    using Code;
    using IObject.Account;
    using IObject.Common;
    using IObject.Common.Account;
    using IObject.Common.Interfaces;
    using IObject.Common.Rep;

    [Authorize]
    public class HomeController : BaseController
    {
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RepAccountsGrid(string searchTxt)
        {
            var context = System.Web.HttpContext.Current;
            request = context.Request;
            response = context.Response;
            response.BufferOutput = true;

            cookieManager = new RepCookieHandler(request, response, 6, domain, secureDomain);
            var aar = cookieManager.BuildRepAccessRequest();
            aar.MaxResults = 5000;
            aar.SearchTxt = searchTxt;

            var res = ExecuteRepAction(aar,
                (client, req) => client.FindAccountsAssignedToRep(req),
                true);

            return PartialView(res.ResponseData);
        }

        public ActionResult LoginAs(int id)
        {
            var context = System.Web.HttpContext.Current;
            request = context.Request;
            response = context.Response;
            response.BufferOutput = true;

            aCookieManager = new AccountCookieHandler(request, response, 6, domain, secureDomain);
            aCookieManager.ClearCookies();
            var aar = aCookieManager.BuildAccountRequest();
            aar.AccountID = id;
            aar.RepRequested = true;

            var res = ExecuteAccountAction(aar,
                (client, req) => client.AuthenticateAccount(req),
                true);

            if (res.ResultCode == (int) Lookup.AccountRequestResultCode.Success)
            {
                return Redirect("http://wgc.dev.com/");
            }
            aCookieManager.ClearCookies();
            return Redirect("http://wgc.dev.com/");
        }

        public ActionResult RepAccounts()
        {
            var context = System.Web.HttpContext.Current;
            request = context.Request;
            response = context.Response;
            response.BufferOutput = true;

            cookieManager = new RepCookieHandler(request, response, 6, domain, secureDomain);
            var aar = cookieManager.BuildRepAccessRequest();

            var res = ExecuteRepAction(aar,
                (client, req) => client.FindAccountsAssignedToRep(req),
                true);

            return View(res.ResponseData);
        }
    }
}