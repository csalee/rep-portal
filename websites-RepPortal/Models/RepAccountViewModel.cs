﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace websites_RepPortal.Models
{
    public class RepAccountViewModel
    {
        public int AccountId { get; set; }
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string HomePhoneNo { get; set; }
    }
}