﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(websites_RepPortal.Startup))]
namespace websites_RepPortal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
